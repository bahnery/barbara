<?php
    include_once 'Component.php';

    class Charts extends Component {
        
        /** 
         *Variáveis para cada parte do gráfico. 
         */
        private $labels = '';
        private $title = '';
        private $data = '';
        private $type = '';
        
        /**Função que chama o gráfico completo e retorna pro controller. */
        public function getHTML(){
            $html = $this->chart();


            return $html;
        }

        /**Função que monta o gráfico com todas as partes possíveis de edição.*/
        public function chart(){
            $html = $this->labels;
            $html .= $this->title;
            $html .= $this->data;
            $html .= $this->type;

            return $html;
            
        } 

        /**
         * Método responsável pela legenda dos dados do gráfico.
         */
        public function addlabels($labels){
            $this->labels .= "$labels";
            return $this;
        }
        public function getlabels(){
            return $this->labels;
        }


        /**Método responsável pelo título do gráfico. */
        public function addtitle($title){
            $this->title .= "$title";
            return $this;
        }

        /**Método responsável pelos dados do gráfico. */
        public function adddata($data){
            $this->data .= "$data";
            return $this;
        }

        /**Método responsável pelo tipo do gráfico. */
        public function addtype($type){
            $this->type .= "$type";
            return $this;
        }
        public function gettype(){
            return $this->type;
        }
    }