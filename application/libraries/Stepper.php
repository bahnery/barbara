<?php
    include_once 'Component.php';
    
    class Stepper extends Component {
        /** Contém a descrição de um passo ativo. */
        private $stepper_description;

        /** Contém as variáveis usadas para gerar o stepper. */
        private $stepper_label = '';
        private $stepper_class = '';
        private $stepper_number = '';
        private $stepper_class_description = '';

        public function __construct($stepper_description){
            $this->stepper_description = $stepper_description; 
        }    

 
        public function getHTML(){
            $html = $this->stepper();
            return $html;
        }

        /**Função responsável pela criação do stepper.
        */
        private function stepper(){
			$html = '<li class="'.$this->stepper_class.'">
				
						<!--Section Title -->
						<a href="#!">
							<span class="circle">'.$this->stepper_number.'</span>
							<span class="label">'.$this->stepper_label.'</span>
						</a>
				
						<!-- Section Description -->
						<div class="'.$this->stepper_class_description.'">
							<p>'.$this->stepper_description.'</p>
						</div>
						</li>';
            return $html;
        }

        /**Função responsável pela adição da classe. Esperado: active, warning, completed, ou deixar em branco.
         */
        public function addStepperClasses($class){
            $this->stepper_class .= "$class";
            return $this;
        }
        public function getstepperclass(){
            return $this->stepper_class;
        }

		/**Função responsável pelo número do passo. Recebe um inteiro. */
        public function addStepNumber($number){
            $this->stepper_number .= $number;
            return $this;
        }
        public function getsteppernumber(){
          return $this->stepper_number;
    	}

		/**Função responsável pelo título do passo. Recebe uma string. */
    	public function addStepLabel($label){
        	$this->stepper_label .= "$label";
        	return $this;
    	}
    	public function getstepperlabel(){
      		return $this->stepper_label;
		}
		
		/**Função responsável pela descrição caso o passo esteja ativo. Adicionar 'step-content' + 'cor desejada'. */
		public function addClassDescription($class_description){
        	$this->stepper_class_description .= "$class_description";
        	return $this;
    	}
    	public function getclassdescription(){
      		return $this->stepper_class_description;
  		}
}