<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('AlertButtonModel', 'button');
		$this->load->model('ChartsModel', 'charts');
		$this->load->model('StepperModel', 'stepper');
	}

	public function index()
	{	
		/**Método inicial, intro. Apresentação da Atividade. */
		$this->load->view('common/header');
		$this->load->view('common/navbar_home');
		$this->load->view('home/intro');
		$this->load->view('common/footer');
	}

	public function alertbutton(){
		/**Método para mostrar os Alert Buttons. */
		$data['button1'] = $this->button->button1();
		$data['button2'] = $this->button->button2();
		$data['button3'] = $this->button->button3();
		$data['button4'] = $this->button->button4();
		$data['button5'] = $this->button->button5();
		$html = $this->load->view('alertbutton', $data, true);
		$this->show($html);
	}
	

	public function charts()
	{
		/**Método para mostrar os gráficos. */
		$data['title'] = $this->charts->addtitle();
		$data['labels'] = $this->charts->addlabels();
		$data['data'] = $this->charts->adddata();
		$data['type'] = $this->charts->addtype();	
		$html = $this->load->view('charts', $data, true);	
		$this->show($html);
	}

	/**Método para mostrar o Stepper.*/
	public function stepper(){	
		$data['step_active'] = $this->stepper->step_active();
		$data['step_completed'] = $this->stepper->step_completed();
		$data['step_warning'] = $this->stepper->step_warning();
		$data['step_default'] = $this->stepper->step_default();
		$html = $this->load->view('stepper', $data, true);
		$this->show($html);
	}

}

