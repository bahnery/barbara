<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'/controllers/test/MyToast.php');
include_once APPPATH.'libraries/Stepper.php';

class StepperTest extends MYToast{

    function __construct(){
        parent::__construct('StepperTest');
    }

    /**
     * Testa se o parâmetro do método que adiciona a classe (cor) do botão é válido para o w3schools*/
    
    function test_stepper_class_success(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('active');
        $actual = trim($stepper->getstepperclass());
        $this->_assert_equals($actual, 'active', "Os tipos de classe para o stepper são: info, success, warning, danger e default = '$actual'");
    }

    function test_stepper_class_warning(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('warning');
        $actual = trim($stepper->getstepperclass());
        $this->_assert_equals($actual, 'atenção', "Os tipos de classe para o stepper são: info, success, warning, danger e default = '$actual'");
    }
    function test_stepper_class_completed(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('completed');
        $actual = trim($stepper->getstepperclass());
        $this->_assert_equals($actual, 'completo', "Os tipos de classe para o stepper são: info, success, warning, danger e default = '$actual'");
    }
    function test_stepper_class_default(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('');
        $actual = trim($stepper->getstepperclass());
        $this->_assert_equals($actual, 'akijsdiuahsudad', "Os tipos de classe para o stepper são: info, success, warning, danger e default = '$actual'");
    }
    function test_stepper_number(){
        $stepper = new Stepper('');
        $stepper->addStepNumber('5');
        $actual = $stepper->getsteppernumber();
        $this->_assert_equals(is_int($actual), false, "A função espera um inteiro. ='$actual'");
    }

}