<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'/controllers/test/MyToast.php');
include_once APPPATH.'libraries/AlertButton.php';

class AlertButtonTest extends MYToast{

    function __construct(){
        parent::__construct('AlertButtonTest');
    }

    /**
     * Testa se o parâmetro do método que adiciona a classe (cor) do botão é válido para o w3schools*/
    
    function test_button_class_success(){
        $alertbutton = new AlertButton('');
        $alertbutton->addButtonClasses('success');
        $actual = trim($alertbutton->getbuttonclass());
        $this->_assert_equals($actual, 'success', "Os tipos de botão são: info, success, warning, danger e default = '$actual'");
    }

    function test_button_class_info(){
        $alertbutton = new AlertButton('');
        $alertbutton->addButtonClasses('info');
        $actual = $alertbutton->getbuttonclass();
        $this->_assert_equals($actual, 'informação', "Os tipos de botão são: info, success, warning, danger e default");
    }

    function test_button_class_warning(){
        $alertbutton = new AlertButton('');
        $alertbutton->addButtonClasses('warning');
        $actual = $alertbutton->getbuttonclass();
        $this->_assert_equals($actual, 'atenção', "Os tipos de botão são: info, success, warning, danger e default");
    }

    function test_button_class_danger(){
        $alertbutton = new AlertButton('');
        $alertbutton->addButtonClasses('danger');
        $actual = $alertbutton->getbuttonclass();
        $this->_assert_equals($actual, 'perigo', "Os tipos de botão são: info, success, warning, danger e default");
    }

    function test_button_class_default(){
        $alertbutton = new AlertButton('');
        $alertbutton->addButtonClasses('default');
        $actual = $alertbutton->getbuttonclass();
        $this->_assert_equals($actual, 'default', "Os tipos de botão são: info, success, warning, danger e default");
    }
}