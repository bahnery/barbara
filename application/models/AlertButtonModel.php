<?php 
include_once APPPATH.'libraries/AlertButton.php';
    
class AlertButtonModel {
    /**Botões gerados para exemplificar na tela do site para o usuário. */
    public function button1(){
        $button = new AlertButton('Success');
        $button->addButtonClasses('success');

        return $button->getHTML();
    }
    public function button2(){
        $button = new AlertButton('Info');
        $button->addButtonClasses('info');

        return $button->getHTML();
    }

    public function button3(){
        $button = new AlertButton('Warning');
        $button->addButtonClasses('warning');

        return $button->getHTML();
    }
    public function button4(){
        $button = new AlertButton('Danger');
        $button->addButtonClasses('danger');

        return $button->getHTML();
    }
    public function button5(){
        $button = new AlertButton('Default');
        $button->addButtonClasses('default');

        return $button->getHTML();
    }
}
