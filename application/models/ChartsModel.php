<?php 
include_once APPPATH.'libraries/Charts.php';
    
class ChartsModel {

    /**
     * Essa função é responsável pelo rótulo dos dados do gráfico. 
     * Importante preencher cada valor acompanhado de aspas duplas e separar por vírgula.
     */
    public function addlabels(){
        $charts = new Charts();
        $charts->addlabels('"Teste 1", "Teste 2", "Teste 3", "Teste 4", "Teste 5"');
        return $charts->getHTML();
    }

    /**
     * Função responsável por escolher o tipo de apresentação do gráfico.
     * Opções: line, bar, horizontalBar, polarArea, doughtnut e pie. (Respeitar sensitive case) 
     */
    public function addtype(){
        $charts = new Charts();
        $charts->addtype('pie');
        return $charts->getHTML();
    }

    /**
     * Função responsável pelos dados apresentados no gráfico.
     * Preencher separado por vírgula. Não é necessário usar aspas duplas.
     */
    public function adddata(){

        $charts = new Charts();
        $charts->adddata('300, 50, 85, 100, 225');
        return $charts->getHTML();
    }

    /**
     * Função responsável pelo título do gráfico.
     *
     */
    public function addtitle(){
        $charts = new Charts();
        $charts->addtitle('# de Testes');
        return $charts->getHTML();
    }

}
