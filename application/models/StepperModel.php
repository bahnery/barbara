<?php 
include_once APPPATH.'libraries/Stepper.php';
    
class StepperModel {
    /**Botões gerados para exemplificar na tela do site para o usuário. */
    public function step_active(){
        $stepper = new Stepper('Descrição sjdjaalk askdjasjd skjdajsd');
        $stepper->addStepperClasses('active');
        $stepper->addStepNumber(2);
        $stepper->addStepLabel('Teste');
        $stepper->addClassDescription('step-content grey lighten-3');

        return $stepper->getHTML();
    }

    public function step_warning(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('warning');
        $stepper->addStepNumber(3);
        $stepper->addStepLabel('Teste');
        $stepper->addClassDescription('');

        return $stepper->getHTML();
    }

    public function step_completed(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('completed');
        $stepper->addStepNumber(1);
        $stepper->addStepLabel('Teste');
        $stepper->addClassDescription('');

        return $stepper->getHTML();
    }

    public function step_default(){
        $stepper = new Stepper('');
        $stepper->addStepperClasses('');
        $stepper->addStepNumber(4);
        $stepper->addStepLabel('Teste');
        $stepper->addClassDescription('');

        return $stepper->getHTML();
    }

    
}