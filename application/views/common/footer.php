
<footer class="page-footer font-small footer-copyright text-center py-3 fixed-bottom unique-color-dark">© Instituto Federal de São Paulo</footer>

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?= base_url('assets/mdb/js/jquery-3.4.1.min.js')?>"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?= base_url('assets/mdb/js/popper.min.js')?>"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/mdb/js/bootstrap.min.js')?>"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?= base_url('assets/mdb/js/mdb.min.js')?>"></script>
<!-- Stepper JavaScript -->
<script type="text/javascript" src="js/addons-pro/stepper.js"></script>
<!-- Stepper JavaScript - minified -->
<script type="text/javascript" src="js/addons-pro/stepper.min.js"></script>
<!-- Initializations -->
<script type="text/javascript">
// Animations initialization
new WOW().init();
$(document).ready(function () {
$('.stepper').mdbStepper();
})
</script>
</body>

</html>
