
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark unique-color-dark">
      <!-- Brand -->
    <div class="container">    
        <a class="navbar-brand" href="https://mdbootstrap.com/docs/jquery/" target="_blank">
            <strong></strong>
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Left -->
            <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= site_url('welcome/')?>">Home
                <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url('welcome/alertbutton')?>">Alert Buttons</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url('welcome/charts')?>">Charts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url('welcome/stepper')?>">Stepper</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url('docs/api/index.html')?>">PHPDoc</a>
            </li>
            </ul>
        </div>
    </div>
  </nav>
  <!-- Navbar -->