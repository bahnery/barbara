<div class="row">
                <div class="col-md-5"><h1 class="title h1 my-4">Charts - MDBootstrap</h1></div>
                <div class="col-md-4 my-auto"><a href="<?= base_url('test/charttest')?>"><button type="button" class="btn btn-dark btn-sm"><i class="fab fa-android fa-2x" aria-hidden="true"></i></button></a></div>
            </div>               
  <p>Os Charts são gráficos componentes o MDBootstrap, sua utilização é simples e foi feito de uma maneira em que até o tipo do gráfico pode ser alterado.</p>
  <p>É possível mudar as seguintes variáveis referentes ao gráfico:</p>
  <ul><li>Título do gráfico;</li>
  <li>Tipo do gráfico: line, bar, horizontalBar, polarArea, doughtnut e pie;</li>
  <li>Dados;</li>
  <li>Legenda referente aos gráficos;</li></ul>

  <p>Os alterações do gráfico são realizadas direto na model ('ChartsModel.php'), atribuindo a cada método o valor correspondente.</p>
  <h3 class="title h3 my-4">Como preencher as variáveis</h3>
  <h5 class="h5 my-4">Title</h5>
  <p>Adicionar uma string de modo simples com o título desejado. <b>Exemplo: '# de Testes'.</b></p>
  <h5 class="h5 my-4">Dados</h5>
  <p>Adicionar uma string com aspas simples, separando os dados por vírgula ( , ). <b>Exemplo: '300, 50, 85, 100, 225'.</b></p>
  <h5 class="h5 my-4">Labels</h5>
  <p>As labels são as legendas referente aos dados. Seu preenchimento é simples, é uma string. Começando por aspas simples, e declarar as legendas acompanhadas de aspas duplas e separadas por vírgula. <b>Exemplo: '"Teste 1", "Teste 2", "Teste 3", "Teste 4", "Teste 5"'.</b></p>
  <h5 class="h5 my-4">Type</h5>
  <p>Adicionar uma string de modo simples com um dos seguintes valores: pie, horizontalBar, line, radar, polarArea, doughnut. <b><i>Importante respeitar os caracteres minúsculos/maiúsculos.</i></b> <b>Exemplo: 'pie'.</b></p>
  <p>Exemplo de Gráfico gerado com o valor das variáveis explicadas acima:</p>
  </div><!-- Grid container -->
  <div class="container mt-5 mb-5">

    <!--Grid row-->
    <div class="row d-flex justify-content-center">

      <!--Grid column-->
      <div class="col-md-10 mb-5">
          <canvas id="myChart"></canvas>
      </div>
      <!--Grid column-->

    </div>
    <!--Grid row-->

  </div>
  <!-- Grid container -->


<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
    type: '<?php echo $type ?>',
    data: {
    labels: [<?php echo $labels ?>],
    datasets: [{
    label: '<?php echo $title  ?>',
    data: [<?php echo $data ?>],
    backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
    'rgba(255,99,132,1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1
    }]
    },
    options: {
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    }
    });
</script>


