 
    <style>
        .btn {
            border: none; /* Remove borders */
            color: white; /* Add a text color */
            padding: 14px 28px; /* Add some padding */
            cursor: pointer; /* Add a pointer cursor on mouse-over */
        }
        
        .success {background-color: #4CAF50;} /* Green */
        .success:hover {background-color: #46a049;}
        
        .info {background-color: #2196F3;} /* Blue */
        .info:hover {background: #0b7dda;}
        
        .warning {background-color: #ff9800;} /* Orange */
        .warning:hover {background: #e68a00;}
        
        .danger {background-color: #f44336;} /* Red */
        .danger:hover {background: #da190b;}
        
        .default {background-color: #e7e7e7; color: black;} /* Gray */
        .default:hover {background: #ddd;}
    </style>
            <div class="row">
                <div class="col-md-6"><h1 class="title h1 my-4">Alert Buttons - W3Schools </h1></div>
                <div class="col-md-4 my-auto"><a href="<?= base_url('test/alertbuttontest')?>"><button type="button" class="btn btn-dark btn-sm"><i class="fab fa-android fa-2x" aria-hidden="true"></i></button></a></div>
            </div>
                <p>Os Alert Buttons, são componentes de fácil utilização.
                Para utilizá-los, é necessário criar um método no arquivo 'AlertButtonModel.php', usando os exemplos como base.
                A primeira linha do método instancia a Classe e recebe o valor referente ao texto do botão.
                Já o segundo valor, é preenchido com a classe da tag.</p>
                <p>Opções de preenchimento do atributo "class":</p>
                <ul><li>success;</li></p>
                <li>info;</li></p>
                <li>warning;</li></p>
                <li>danger;</li></p>
                <li>default;</li></ul>
                Como mostrados nos exemplos abaixo: </p>
                <div class="container">
                    <div class="text-center">
                        <?php echo $button1, $button2, $button3, $button4, $button5;
                        ?>
                    </div>
                </div>