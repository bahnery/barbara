  <!-- Full Page Intro -->
  <div class="view" style="background-image: url('<?= base_url('assets/mdb/img/start.jpg') ?>');background-repeat: no-repeat; background-size: cover; background-position: center center;">

    <!-- Mask & flexbox options-->
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

      <!-- Content -->
      <div class="container">
        <!--Grid row-->
        <div class="row pt-lg-5 mt-lg-5">
          <!--Grid column-->
          <div class="col-md-6 mb-5 mt-md-0 mt-5 white-text text-center text-md-left wow fadeInLeft"
            data-wow-delay="0.3s">
            <h1 class="display-5 font-weight-bold">Atividade 2 - Modelando Componentes</h1>
            <hr class="hr-light">
            <h6 class="mb-3">Bárbara Nery Araújo - GU3003795<br/>
              Prof° Reginaldo do Prado
            </h6>
          
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->

    </div>
    <!-- Mask & flexbox options-->

    </div>
  <!-- Full Page Intro -->