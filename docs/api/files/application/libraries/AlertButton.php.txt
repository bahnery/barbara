<?php
    include_once 'Component.php';
    
    class AlertButton extends Component {
        /** Contém o texto do botão */
        private $texto;

        /** Contém as classes do botão */
        private $button_classes = '';

        public function __construct($texto){
            $this->texto = $texto; 
        }    

 
        public function getHTML(){
            $html = $this->button();
            return $html;
        }

        /**Função responsável pela criação do botão.
        */
        private function button(){
            $html = '<button class="btn btn-'.$this->button_classes.'">'.$this->texto.'</button>';
            return $html;
        }

        /**Função responsável pela adição das classes desejaveis no botão.
         */
        public function addButtonClasses($class){
            $this->button_classes .= "$class ";
            return $this;
        }

        public function getbuttonclass(){
            return $this->button_classes;
        }
    }
