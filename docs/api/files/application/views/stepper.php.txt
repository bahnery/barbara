<div class="row">
    <div class="col-md-6"><h1 class="title h1 my-4">Stepper - MDBootstrap </h1></div>
    <div class="col-md-4 my-auto"><a href="<?= base_url('index.php/test/steppertest')?>"><button type="button" class="btn btn-dark btn-sm"><i class="fab fa-android fa-2x" aria-hidden="true"></i></button></a></div>
</div>
<p>O Stepper é um componente para fasear em passo a passo os processos.
Para utilizá-los, é necessário criar um método no arquivo 'StepperModel.php', usando os exemplos como base.</p>
<p>Foram criados 4 métodos para diferenciar visualmente as classes dos Passos.
A descrição do Step é feita através do <b>__construct</b>, por isso, caso o passo haja alguma descrição, deverá instanciar a classe passando como paramêtro a descrição em string.</p>
<p>O método <b>addStepClasses</b> é referente a classe do stepper, que indica se o passo está Ativo, Pendente, Completo ou com Erro.</p>
<ul><li>Ativo: 'active';</li></p>
<li>Pendente: deixar em branco!;</li></p>
<li>Erro: 'warning';</li></p>
<li>Completo: 'completed';</li></p></ul>
<p><b>addStepNumber</b>: adiciona o número do passo.
<p><b>addStepLabel</b>: adiciona o título do passo.
<p><b>addClassDescription</b>: adiciona a descrição caso o passo esteja ativo, ou o usuário deseje uma descrição. Importante lembrar, que a ClassDescription por padrão recebe o valor 'step-content' adicionada de uma cor do bootstrap escolhida pelo usuário, como por exemplo 'grey lighten-3'.</p>
<!-- Vertical Steppers -->
<div class="row mt-1">
  <div class="col-md-12">
    <ul class="stepper stepper-vertical">
    Segue exemplo de Stepper abaixo: 
      <?php echo $step_completed;
            echo $step_active;
            echo $step_warning;
            echo $step_default;?>

    </ul>
    <!-- /.Stepers Wrapper -->
    </div>
</div>
