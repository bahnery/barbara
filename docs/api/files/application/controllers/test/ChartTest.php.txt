<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'/controllers/test/MyToast.php');
include_once APPPATH.'libraries/Charts.php';

class ChartTest extends MYToast{

    function __construct(){
        parent::__construct('ChartTest');
    }

    // Caso de teste 1

    function test_chart_type_bar(){
        $charts = new Charts();
        $charts->addtype('bar');
        $actual = $charts->gettype();
        $this->_assert_equals($actual, 'barra', "Os tipos de gráfico são: line, bar, horizontalBar, polarArea, doughtnut e pie");
    }

    function test_chart_type_line(){
        $charts = new Charts();
        $charts->addtype('line');
        $actual = $charts->gettype();
        $this->_assert_equals($actual, 'linha', "Os tipos de gráfico são: line, bar, horizontalBar, polarArea, doughtnut e pie");
    }

    function test_chart_type_horizontalBar(){
        $charts = new Charts();
        $charts->addtype('horizontalBar');
        $actual = $charts->gettype();
        $this->_assert_equals($actual, 'horizontalbar', "Os tipos de gráfico são: line, bar, horizontalBar, polarArea, doughtnut e pie");
    }

    function test_chart_type_polarArea(){
        $charts = new Charts();
        $charts->addtype('polarArea');
        $actual = $charts->gettype();
        $this->_assert_equals($actual, 'areapolar', "Os tipos de gráfico são: line, bar, horizontalBar, polarArea, doughtnut e pie");
    }

    function test_chart_type_doughtnut(){
        $charts = new Charts();
        $charts->addtype('doughtnut');
        $actual = $charts->gettype();
        $this->_assert_equals($actual, 'dougnut', "Os tipos de gráfico são: line, bar, horizontalBar, polarArea, doughtnut e pie");
    }

    function test_chart_type_pie(){
        $charts = new Charts();
        $charts->addtype('pie');
        $actual = $charts->gettype();
        $this->_assert_equals($actual, 'pie', "Os tipos de gráfico são: line, bar, horizontalBar, polarArea, doughtnut e pie");
    }

}
